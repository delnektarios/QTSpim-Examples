#################################################
# 						#
# lab4_1a.s					#
# stack exercise 1 (to be completed)		#
#						#
#################################################
	.text		
       	.globl __start 
__start:			# execution starts here

# start of main program
	#li $a0,-10		# Initilize variables
	#li $a1,-30		#
	li $a2,120		#
	li $a3,200		#
	li $s0,1000
	jal maximum
	move $t0,$v0
	move $t1,$v1
	la $a0,max		
	li $v0,4
	syscall			# display "Max is :"
	move $a0,$t0		
	li $v0,1
	syscall			# display max
	la $a0,endl		
	li $v0,4
	syscall			# display end of line
	la $a0,min		
	li $v0,4
	syscall			# display "Min is :"
	move $a0,$t1		
	li $v0,1
	syscall			# display min
	la $a0,endl		
	li $v0,4
	syscall			# display end of line
	li $v0,10		
	syscall			# exit 
# end of main programm

# start of procedure
maximum:
	slt $s0,$a2,$a3
	beq $s0,1,next
	add $v0,$a3,$zero
	add $v1,$a2,$zero
	next: add $v0,$a2,$zero
	add $v1,$a3,$zero
	jr $ra
	
	
# end of procedure	

		.data
max:		.asciiz "Max is : "
min:		.asciiz "Min is : "
endl:		.asciiz "\n"
#################################################
# 						#
# End of program				# 
#						#
#################################################