#################################################
# 						#
# lab4_2a.s					#
# stack exercise 2 (to be completed)		#
#						#
#################################################
	.text		
       	.globl __start 
__start:			# execution starts here

# start of main program

	la $a0,prompt		
	li $v0,4
	syscall			# display "Enter integer number :"		
	li $v0,5
	syscall			# read integer
	move $t0,$v0
	la $a0,endl		
	li $v0,4
	syscall			# display end of line
	move $a0,$t0

	li $v0,10		
	syscall			# exit 
# end of main program

# start of procedure
#
# end of procedure	

		.data
prompt:		.asciiz "Enter integer number :"
endl:		.asciiz "\n"
#################################################
# 						#
# End of program				# 
#						#
#################################################