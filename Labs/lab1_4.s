#################################################
# 						#
# lab1_4.s					# 
# read a string and print its first 5 chars	#
#						#
#################################################

#################################################
#					 	#
#		text segment			#
#						#
#################################################

	.text		
       	.globl __start 
__start:			# execution starts here
	
	li $v0,8		# code to read a string
	li $a1,20		# max size = 20 bytes
	la $a0,str 		# $a0 points to the string
	syscall
	
	sb $zero,5($a0)

	li $v0,4
	syscall
	

	li $v0,10
	syscall 		# au revoir...


#################################################
#					 	#
#     	 	data segment			#
#						#
#################################################

.data
str: 	.asciiz		"------------------------"
endl: 	.asciiz 	"\n"

#################################################
# 						#
# End of File					# 
#						#
#################################################