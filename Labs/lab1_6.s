#################################################
# 						#
# lab1_4.s					# 
# reads two integers and prints their suma and differenece #
#						#
#################################################

#################################################
#					 	#
#		text segment			#
#						#
#################################################

	.text		
       	.globl __start 
__start:			# execution starts here
	
	li $v0,5		# code to read an integer
	syscall

	add $t1,$v0,$zero	# $t1 is the first number

	li $v0,5		# code to read an integer
	syscall

	add $t2,$v0,$zero	# $t2 is the first number

	add $t3,$t1,$t2		# $t3 is the sum
	sub $t4,$t1,$t2		# $t4 is the difference


	add $a0,$t3,$zero	# move $t3 to $a0 to print it
	li $v0, 1
	syscall

	la $a0,endl
	li $v0, 4
	syscall

	add $a0,$t4,$zero	# move $t4 to $a0 to print it
	li $v0, 1
	syscall

	li $v0,10
	syscall 		# au revoir...


#################################################
#					 	#
#     	 	data segment			#
#						#
#################################################

.data
str: 	.asciiz		"------------------------"
endl: 	.asciiz 	"\n"

#################################################
# 						#
# End of File					# 
#						#
#################################################